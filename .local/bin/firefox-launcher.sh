#!/usr/bin/bash 
ffcmd=/opt/firefox/firefox-bin

ws=$(swaymsg -r -t get_workspaces| jq -r '.[]|select(.focused)|.name')
ff=$(swaymsg -r -t get_tree| jq -r '..|select(.type?=="workspace" and .name?=="'$ws'")|.nodes[],.floating_nodes[]|select(.app_id=="firefox")|.app_id'|head -1)

if [[ $ff == "firefox" ]]; then
  $ffcmd $1
else
  $ffcmd --new-window $1
fi

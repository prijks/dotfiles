#!/usr/bin/bash
# based on https://github.com/Alexays/Waybar/pull/85#issuecomment-560100037

swaymsg -t get_inputs | jq -rcM --argjson icons '["🇺🇸","🇩🇪"]' \
    "first(.[] | select(.identifier == \"$1\" and .type == \"keyboard\")) \
    | \$icons[.xkb_active_layout_index]"

swaymsg -mrt subscribe '["input"]' | jq -rcM --unbuffered --argjson icons '["🇺🇸","🇩🇪"]' \
    "select(.change == \"xkb_layout\")
    | .input
    | select(.identifier == \"$1\" and .type == \"keyboard\") \
    | \$icons[.xkb_active_layout_index]"

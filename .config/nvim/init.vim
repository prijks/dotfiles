"
" Pete's neovim config
"

set expandtab
set shiftwidth=2
set tabstop=2
set list
set listchars=tab:▸\ ,trail:·
set scrolloff=2

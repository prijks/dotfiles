#!/bin/bash

swaymsg "workspace 1"
sleep 0.2s
kitty --class=kitty-ws1-calcurse calcurse&
until swaymsg -r -t get_tree|grep kitty-ws1-calcurse; do :; done
swaymsg "splith"
kitty --class=kitty-ws1-alpine alpine&
until swaymsg -r -t get_tree|grep kitty-ws1-alpine; do :; done
swaymsg "[app_id=kitty-ws1-calcurse] focus"
swaymsg "splitv"
kitty --class=kitty-ws1-ncmpcpp ncmpcpp&
until swaymsg -r -t get_tree|grep kitty-ws1-ncmpcpp; do :; done
swaymsg "[app_id=kitty-ws1-alpine] focus"
swaymsg "splitv"
kitty --class=kitty-ws1-ies --title="InefficientFutileSearch" bash -c "cd ~/cse/pn/InefficientFutileSearch && ./factorpn" &
until swaymsg -r -t get_tree|grep kitty-ws1-ies; do :; done
swaymsg "[app_id=kitty-ws1-alpine] resize set height 800px"
swaymsg "[app_id=kitty-ws1-calcurse] resize set height 660px"
swaymsg "[app_id=kitty-ws1-alpine] focus"

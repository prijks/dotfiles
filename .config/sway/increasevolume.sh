#!/bin/bash
vol=$(pulsemixer  --get-volume|cut -f 1 -d ' ')
if [[ $vol -lt 100 ]]
then
    pactl set-sink-volume @DEFAULT_SINK@ +2%
fi

#!/bin/bash

date_formatted=$(date +'%Y-%m-%d %k:%M:%S')
volume=$(pulsemixer  --get-volume|cut -f 1 -d ' ')

echo "$volume%  |  $date_formatted"


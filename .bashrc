# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files
# (in the package bash-doc) for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

set -o vi
bind -m vi-command 'Control-l: clear-screen'
bind -m vi-insert 'Control-l: clear-screen'

if [ -x /usr/bin/dircolors ]; then
  test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
  alias ls='ls --color=auto'
fi

PROMPT_COMMAND=__prompt_command
__prompt_command() {
    local EXIT="$?"

    case "$TERM" in
      xterm*|rxvt*)
        PS1="\[\033]0;\]\u@\h: \w\[\007\]"
        ;;
      *)
        PS1=""
        ;;
    esac

    PS1+='[\[\033[1;'
    PS1+='34m\]\t\[\033[0'
    PS1+='m\]] \u@\h [\w]\n'
    if [ $EXIT != 0 ]; then
      PS1+="\[\033[31"
    else
      PS1+="\[\033[32"
    fi

    PS1+="m\]prompt-fu\[\033[0"
    PS1+="m\]\$ "
}


case $TERM in
  xterm*|rxvt*)
    set -o functrace
    show_command_in_title_bar()
    {
      case "$BASH_COMMAND" in
        *\033]0*)
          ;;
        *)
          echo -ne "\033]0;${BASH_COMMAND}\007"
          ;;
      esac
    }
    trap show_command_in_title_bar DEBUG
    ;;
  *)
    ;;
esac

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

if [ -d "$HOME/.local/bin" ]; then
    PATH="$HOME/.local/bin:$PATH"
fi
